## 1 Getting Started
    * Installation options:
        - CSS and JS files
        - Source files
        - Bootstrap CDN
        - Package manager

## 2 Basic Styles        
    * Basic typoraphy
        - Reboot.css styles
        - Rems vs Ems
        - Avoid margin top
        - Inherit when possible
        - Border-box sizing
        - Native font stacks
        - Special styles
    * Typographic utilities
        - Alignment utilities
            + text-justify: give element sort of fit to the left & the right with same amount of spacing
            + text-nowrap: make the text never wrapped around
            + text(-xx)-pos
                . XX: sm > 576px, md > 768px, lg > 992px, xl > 1200px
                . pos: left, center, right
    * Blockquote & List
        - List
            + list-unstyled: no bullet
            + Inline list:
                . list-inline: on ul
                . list-inline-item: on each li        
        - Blockquote:
            + class blockquote
            + blockquote-reverse: text align right instead of left
            + blockquote-footer:
    * Color:   
        - Primary: blue
        - Success: green
        - Info: light blue
        - Warning: orange
        - Danger: red
    * Images:
        - Classes:
            + img-fluid: responsive image
            + rounded: rounded image
            + img-thumbnail: rounded 1px border image
        - Align:    
            + float-left, float-right   
            + text-center
            + mx-auto         
        - Figure:
            + figure class on <figure>  
            + figure-img: for image
            + figure-caption: on text                     

## 3 Layout            
    * Colum grid
        - Extra small
        - small
        - Medium
        - Large 
        - Extra large
    * The grid:
        - 12 col responsive 
        - Flexbox based
        - Structure:
            + Container
            + Rows/columns
    * Grid Container:
        - container
        - container-fluid
        - 15px padding
        - adjust to breakpoints: <576 576 768 992 1200px            
    * Colums:
        - Can expand
        - sm > 576, md > 768, lg > 992, xl > 1200px
        - col: 1-12
    * Colums offset: move a column on the grid
        -
        - offset-BP-COL
        - BP : sm > 576, md > 768, lg > 992, xl > 1200px
        - COL: 1-11
    * Nested column
        - row inside column
        - Create 12-col grid inside column
        - May use same classes
    * Custome columns order
        - Flex order
            + flex-(BP)-ORD
                . BP : sm > 576, md > 768, lg > 992,xl > 1200px
                . ORD: first, last, unordered
        - Push & Pull
            - Push: move col to right
            - Pull: move col to left
            - Act-(BP)-(ORD)
                . Act: push / pull                
                . BP : sm > 576, md > 768, lg > 992,xl > 1200px 
                . ORD: 1-12           
    * Grid alignment options   
        - Vertical:
            + Use in rows     
            + align-items-aln
                . aln: start, center, end
            + Work on nested tools
        - Individual:
            + Use in cols
            + align-self-aln
                . aln: start, center, end
        - Horizontal:
            + Use in rows        
            + Need col width
            + justify-content-aln
                . aln: start, center, end, around, between
    * Display:
        - Position:     
            + fixed-top, fixed-bottom, sticky-top   
            + sticky-top lack support
        - Basic display:
            + Mimic css
            + d-type: block, inline, inline-block, flex
        
    * Flex container:        
        - Basic flex container:
            + d(-BP)(-inline)-flex
                . BP : sm > 576, md > 768, lg > 992,xl > 1200px
        - Direction:
            + flex(-BP)(-DIR)(-reverse)
                . DIR: row, col     
                . BP : sm > 576, md > 768, lg > 992,xl > 1200px    
        - Justify
            + justify-content(-BP)-aln
                . aln: start, center, end, around, between   
                . BP : sm > 576, md > 768, lg > 992,xl > 1200px  
        - Wrap
            + flex(-BP)-WRP(-reverse)
                . BP : sm > 576, md > 768, lg > 992,xl > 1200px
                . WRP: wrap, nowrap
        - Vertical alignment
        - Horizontal alignment  
    * Element spacing & ultilities
        - Float
            + float(-BP)-SID
            + SID: left, right, none
            + clearfix: clear the float
        - Margin/ padding
            + PRO(SID)(-BP)(SIZ)    
                . PRO: m/p (margin/padding)
                . SID: t,r,b,l,x,y
                . SIZ: 0,1,2,3,4,5,auto
        - Sizing:
            + SIZ(-AMT)
                . SIZ: w h mh mw (width, height,max-height, max-width)
                . AMT: 25 50 75 100 (%)
        - Visibility:
            + Invisible: toggles visibility
            + hidden-BP-DIR
                . DIR: up,down     
                . BP : xs <576,sm > 576, md > 768, lg > 992,xl > 1200px
        - Align self        
            + align-self(-BP)-ALG
                ALG: start, end, center, baseline,stretch   
## 4 Navs & Navbar
    * Navigation component:
        - Nav
        - Pills
        - Tabs
        - Navbar
    * Navbar component:
        - Branding
        - Color schemes
        - Dropdowns
        - Form elements
    * Basic Nav
        - With/without ul
        - nav
        - nav-item
        - nav-link:
            + active
            + disable
        - nav styles:
            + nav-pills
            + nav-tabs
        - nav alignment:
            + justify-content-end
            + justify-content-center
            + nav-fill
            + nav-justified
            + flex-column
    * Navbar:
        - navbar
        - navbar-toggleable-BP:
            + BP : xs <576,sm > 576, md > 768, lg > 992,xl > 1200px
        - navbar-nav
            + nav-item
            + nav-link
            + active
            + disable
        - navbar-color
            + bg-color
            + navbar-light
            + navbar-inverse    
    * Navbar brand
        - navbar-brand
        - link or headline  
        - images
        - navbar-text           
    * Dropdown
        - dropdown to align
        - dropdown-toggle on link
        - data-toggle = "dropdown"
        - dropdown-menu
        - dropdown-item
        - id & data attributes    
    * Form element:
        - form-inline
        - form-control
        - space as needed    
    * Placement
        - fixed-top
        - fixed-bottom
        - sticky-top
        - mr-auto    
    * Collapsable content:
        - collapse
        - navbar-collapse
        - id
        - navbar-toggler  
        - other properties
        - navbar-toggler-icon    
## 5 Basic Style elements
    * Button:
        - btn : basic class
        - btn-size: have sm, lg
        - can be <a> <button> <input>
        - color:
            + btn(-color)
            + btn-outline(-color)
            + color: primary,secondary,success, info, warning, danger
         - other option:
            + btn-block: full width       
            + active
            + disabled: prevent from clicking
    * Button group:
        - btn-group
        - btn-group-vertical
        - btn-toolbar        
        - btn-group-size (sm,lg)
    * Badges
        - badge
        - badge-pill
        - badge-color (same with button)    
    * Progress bar
        - progress
        - progress-bar
        - style: width, height
        - label: text
        - bg-color: success,info, warning, danger
        - progress-bar-striped
        - progress-bar-animated
        - multiple bars
        - accessibility properties:
            + role = "progressbar"
            + aria-valuenow
            + aria-valuemin
            + aria-valuemax   
    * List group
        - list-group 
        - list-group-item
        - <li> <button> <a>
        - styles: 
            + active
            + disabled
        - list-group-item-action
        - can add badges
        - justify-content-between       
    * Breadcrumbs:
        - breadcrumb
        - breadcrumb-item      
        - active
        - li nav > a

## 6 Layout Component
    * Jumbotron
        - jumbotron
        - jumbotron-fluid        
    * Table
        - table
        - table-inverse
        - thead-inverse
        - table-striped
        - table-bordered
        - table-hover
        - table-sm
        - table-responsive
        - use table-color on row/td:
            + active
            + success,info,warning,danger    
    * Card
        - card
        - style: 
            + card-color: same with button
            + card-inverse
            + card-outline-color: same with button
        - card-block
        - card-text
        - card-title
        - card-subtitle
        - card-link
        - Images:
            + card-img-top
            + card-img-bottom
            + card-img-overlay         
    * Card content
        - Header & footer
            + card-header
            + card-footer
        - List & tab
            + list-group-flush
            + card-header-tab     
    * Card layout
        - Traditional layout:   
            + col
            + flexbox
        - Layout classes:
            + card-group
            + card-deck
            + card-columns     
    * Media element
        - media
        - media-body
        - flexbox classes     
## 7 Using Form Styles
    * Basic form:
        - form-group
        - form-text
        - form-control
            + form-control (for input)
            + form-control-label
            + form-contorl-file       
    * Checkbox & radio button
        - form-check
        - form-check-label
        - form-check-input
        - form-check-inline
    * Size & validation style:
        - form-control
            + sm
            + lg
        - has-color: same as button
        - form-control-color: same as button    
    * Multi column form    
        - need container
        - use rows, cols
        - use col-form-label
    * Input group
        - input-group-addon
        - add on each side
        - on other element    
## 8 Working with interactive component
    * Tooltips
        - Link or Control
        - Use tether library
        - data-toggle="tooltip"
        - title="text"
        - To control, use JS or data attribute
            ex :
                $(function(){
                    $('data-toggle="tooltip"').tooltip()
                })        
        - Common options:
            + placement: top right bottom left
            + trigger: click, hover, focus
            + html : true, false       
    * Display popover
        - Also use tether library
        - data-toggle="popover"
        - title="text"
        - data-content="content"
        - Control same as tooltip     
    * Alert
        - alert
        - alert-color (same with button)
        - alert content
            + alert-heading
            + alert-link
        - alert dismiss:
            + alert-dismissible fade show
            + Add close button
    * Collapse & Accordions
        - Collapse:
            + Link / Button
            + data-toggle="collapse"
            + id or data-target
            + collapse class
        - Accordion
            + require container
            + show class once
            + dropdown-menu
            + use card    
    * Modal
        - The trigger   
            + Link / Button 
            + id / data-target
        - data-toggle="modal"
        - modal class
        - Structure:
            + modal-dialog
            + modal-content
            + modal-header
            + modal-body
            + modal-footer
        - Modal option:
            + modal-title
            + data-dimiss="modal"            
    * Carousel
        - carousel
        - data-ride="carousel"
        - carousel-inner
        - carousel-item
        - option:
            + active class
            + crop & size photos   
    * Scrollspy
        - data-spy="scroll"
        - position: relative
        - data-target ="id"
        - fixed-top             